ENTRY(_start)

MEMORY {
  dos : org = 0x100, len = (0xFFFF - 0x100)
}

SECTIONS {
  .text   : ALIGN(1) SUBALIGN(1){ *(.startup) *(.text .text.*) }   > dos
  .rodata : ALIGN(1) SUBALIGN(1){ *(.rodata .rodata.*) } > dos
  .data   : ALIGN(1) SUBALIGN(1){ *(.data) }   > dos
  .bss    : ALIGN(1) SUBALIGN(1){ *(.bss) }    > dos
  .stack  : ALIGN(1) SUBALIGN(1){ *(.stack) }  > dos
  .shstrtab : { *(.shstrtab) }
  _heap = ALIGN(4);
  /DISCARD/ :
  {
	*(*)
  }
}
