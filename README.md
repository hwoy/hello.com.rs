# hello.com.rs (Creating a DOS executable with Rust)

It is possible to create a DOS executable or 1st stage bootloader with Rust.  
This is a quick demo of creating COM executable for DOS.

## Building
You need nightly toolchain and rust-src.

```shell
rustup toolchain install nightly
rustup component add rust-src --toolchain nightly
```

Then you can build the project by running:

```shell
cargo build --release
```

### Special Thanks
- i586-rust_dos.json [https://github.com/o8vm/rust_dos](https://github.com/o8vm/rust_dos)

### Contact me
- Web: https://github.com/hwoy 
- Email: zaintcorp@yahoo.com 
- Facebook: http://www.facebook.com/dead-root
