#![no_std]
#![no_main]

use barebone::*;

fn main() -> i32 {
    libc::puts(b"Hello, World!");
    return 0;
}

#[export_cffi]
fn _start() -> ! {
    libc::exit(main())
}

use core::panic::PanicInfo;
#[panic_handler]
fn panic(_: &PanicInfo) -> ! {
    libc::puts(b"Panic!");
    libc::exit(1)
}
