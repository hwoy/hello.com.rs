#![no_std]

use dos::Interupt;

pub fn putchar(ch: i32) -> i32 {
    let mut reg = dos::REG::new();
    reg.edx.value = ch as u32;
    reg.eax.reg16.reg8.h = 0x02;
    reg.int0x21();

    return ch;
}

pub fn puts(s: &[u8]) -> i32 {
    for &i in s.iter() {
        putchar(i as i32);
    }

    putchar(13);
    putchar(10);

    return 0;
}

#[inline(always)]
pub fn exit(code: i32) -> ! {
    let mut reg = dos::REG::new();
    reg.eax.value = code as u32;
    reg.eax.reg16.reg8.h = 0x4c;
    reg.int0x21();
    unsafe { core::hint::unreachable_unchecked() }
}
