#![no_std]

#[derive(Copy, Clone)]
pub struct REG8 {
    pub l: u8,
    pub h: u8,
}

#[derive(Copy, Clone)]
pub union REG16 {
    pub reg8: REG8,
    pub value: u16,
}

#[derive(Copy, Clone)]
pub union REG32 {
    pub reg16: REG16,
    pub value: u32,
}

#[derive(Copy, Clone)]
pub struct REG {
    pub eax: REG32,
    pub ebx: REG32,
    pub ecx: REG32,
    pub edx: REG32,
}

impl REG {
    pub fn new() -> Self {
        Self {
            eax: REG32 { value: 0 },
            ebx: REG32 { value: 0 },
            ecx: REG32 { value: 0 },
            edx: REG32 { value: 0 },
        }
    }
}

pub trait Interupt {
    fn int0x21(&self) -> Self;
}

use core::arch::asm;
impl Interupt for REG {
    fn int0x21(&self) -> Self {
        let mut oreg = REG::new();

        unsafe {
            asm!(
                "int $0x21",
                inout("eax") self.eax.value => oreg.eax.value,
                inout("ebx") self.ebx.value => oreg.ebx.value,
                inout("ecx") self.ecx.value => oreg.ecx.value,
                inout("edx") self.edx.value => oreg.edx.value
            );
        }

        return oreg;
    }
}
